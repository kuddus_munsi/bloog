﻿<?php
include "inc/header.php";
include "inc/sidebar.php"
?>
    <style>
        .left{float: left;width: 70%;}
        .right{float: left;width: 20%}
        .right img{height: 160px;width: 160px;}
    </style>
<div class="grid_10">
		
            <div class="box round first grid">
                <h2>Update Site Title and Description</h2>
               <?php if($_SERVER['REQUEST_METHOD'] == 'POST') {
                $title = $format->validation($_POST['title']);
                $slogan = $format->validation($_POST['slogan']);
                $title = mysqli_real_escape_string($db->link,$title);
                $slogan = mysqli_real_escape_string($db->link, $slogan);


                $permitted = array('jpg' , 'jpeg' , 'png');
                $file_Name = $_FILES['logo']['name'];
                $file_Size = $_FILES['logo']['size'];
                $file_Temp = $_FILES['logo']['tmp_name'];
                $div = explode('.',$file_Name);
                $file_ext = strtolower(end($div));
                $unique_image = 'logo'.'.'.$file_ext;
                $uploaded_image = "upload/".$unique_image;

                if($title == "" OR $slogan == ""){
                echo "<span class='err'>Input field should not be empty</span>";
                }else {
                if (!empty($file_Name)) {
                    if ($file_Size > 1048567) {
                    echo "<span class='err'>Image should be less then 1MB.</span>";
                    } elseif (in_array($file_ext, $permitted) === false) {
                    echo "<span class='err'>you can upload only :-" . implode(',', $permitted) . "</span>";
                    } else {
                    move_uploaded_file($file_Temp, $uploaded_image);
                    $query = "UPDATE title_slogan
                    SET
                    title = '$title',
                    slogan = '$slogan',
                    logo = '$uploaded_image'
                     WHERE  id='1' ";
                    $updated_rows = $db->update($query);
                    if ($updated_rows) {
                    echo "<span class='succes'>Post updated Successfully.</span>";
                    } else {
                echo "<span class='err'>Post Not updated.</span>";
                }
                }
                } else {
                $query = "UPDATE title_slogan
                SET
                    title = '$title',
                    slogan = '$slogan'
                WHERE  id='1'";
                $updated_rows = $db->update($query);
                if ($updated_rows) {
                echo "<span class='succes'>Post updated Successfully.</span>";
                } else {
                echo "<span class='err'>Post Not updated.</span>";
                }
                }


                }

                }
                ?>



                <?php
                    $query = "SELECT * FROM title_slogan WHERE  id = '1'";
                    $allData = $db->select($query);
                    if($allData){
                        while ($data = $allData->fetch_assoc()){

                ?>
                <div class="block sloginblock">
                  <div class="left">
                      <form action="" method="post" enctype="multipart/form-data">

                      <table class="form">
                        <tr>
                            <td>
                                <label>Website Title</label>
                            </td>
                            <td>
                                <input type="text" value="<?php echo $data['title'];?>" name="title" class="medium" />
                            </td>
                        </tr>
						 <tr>
                            <td>
                                <label>Website Slogan</label>
                            </td>
                            <td>
                                <input type="text"value="<?php echo $data['slogan'];?>" name="slogan" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Website Logo</label>
                            </td>
                            <td>
                                <input type="file" placeholder="Enter Website Slogan..." name="logo" class="medium" />
                            </td>
                        </tr>
						
						 <tr>
                            <td>
                            </td>
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                    </form>
                  </div>
                    <div class="right">
                        <img src="<?php echo $data['logo'];?>" alt="">
                    </div>
                </div>
     <?php }} ?>
            </div>
        </div>
<?php include "inc/footer.php";?>