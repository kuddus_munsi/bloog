﻿<?php
include "inc/header.php";
include "inc/sidebar.php"
?>
        <div class="grid_10">
		
            <div class="box round first grid">
                <h2>Add New Post</h2>
             <?php

             if($_SERVER['REQUEST_METHOD'] == 'POST') {
                 $title = mysqli_real_escape_string($db->link, $_POST['title']);
                 $category = mysqli_real_escape_string($db->link, $_POST['category']);
                 $tags = mysqli_real_escape_string($db->link, $_POST['tag']);
                 $author = mysqli_real_escape_string($db->link, $_POST['author']);
                 $body = mysqli_real_escape_string($db->link, $_POST['body']);
                 $userid = mysqli_real_escape_string($db->link, $_POST['userid']);

                 $permitted = array('jpg' , 'jpeg' , 'png' , 'gif');
                 $file_Name = $_FILES['image']['name'];
                 $file_Size = $_FILES['image']['size'];
                 $file_Temp = $_FILES['image']['tmp_name'];
                 $div = explode('.',$file_Name);
                 $file_ext = strtolower(end($div));
                 $unique_image = substr(md5(time()), 0 ,10).'.'.$file_ext;
                 $uploaded_image = "upload/".$unique_image;
                 if($title == "" OR $tags == "" OR $category == "" OR $body == "" OR $author == ""){
                     echo "<span class='err'>Input field should not be empty</span>";
                 }
                 elseif($file_Size > 1048567){
                     echo "<span class='err'>Image should be less then 1MB.</span>";
                 }
                 elseif (in_array($file_ext , $permitted) === false){
                     echo "<span class='err'>you can upload only :-".implode(',' , $permitted)."</span>";
                 }
                 else{
                     move_uploaded_file($file_Temp ,$uploaded_image);
                     $query = "INSERT INTO   post (category,title,body,image,author,tag,userid) VALUES ('$category', '$title', '$body', '$uploaded_image', '$author', '$tags','$userid')";
                     $inserted_rows = $db->Insert($query);
                     if($inserted_rows){
                         echo  "<span class='succes'>Post Uploaded Successfully.</span>";
                     }
                     else{
                         echo "<span class='err'>Post Not Uploaded.</span>";
                     }
                 }
                 }
             ?>
            <div class="block">
                 <form action="" method="post" enctype="multipart/form-data">
                    <table class="form">
                       
                        <tr>
                            <td>
                                <label>Title</label>
                            </td>
                            <td>
                                <input type="text" name="title" placeholder="Enter Post Title..." class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Tags</label>
                            </td>
                            <td>
                                <input type="text" name="tag" placeholder="Enter tags" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label> Author </label>
                            </td>
                            <td>
                                <input type="text" name="author" value="<?php echo Session::get('username')?>" class="medium" />
                                <input type="hidden" name="userid" value="<?php echo Session::get('userID')?>" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Category</label>
                            </td>
                            <td>
                            <select id="select" name="category">
                            <option value="1">Category One</option>

                             <?php
                            $query = "SELECT * FROM category";
                            $catagory  =  $db->select($query);
                            if($catagory){
                                while ($data = $catagory->fetch_assoc()){
                            ?>
                                <option value="<?php echo $data['id'] ?>"><?php echo $data['name'] ?></option>
                                <?php }}?>
                                </select>
                            </td>

                        </tr>
                   
                    

                        <tr>
                            <td>
                                <label>Upload Image</label>
                            </td>
                            <td>
                                <input type="file" name="image" />
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-top: 9px;">
                                <label>Content</label>
                            </td>
                            <td>
                                <textarea class="tinymce" name="body"></textarea>
                            </td>
                        </tr>
						<tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
        </div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php include "inc/footer.php";?>

