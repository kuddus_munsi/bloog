<?php
include "inc/header.php";
include "inc/sidebar.php";

if(!isset($_GET['sliderid']) OR $_GET['sliderid'] == NULL){
    echo "<script>window.location = 'sliderlist.php';</script>";
}else{
    $sliderID = $_GET['sliderid'];
}




?>
<div class="grid_10">

    <div class="box round first grid">
        <h2>Edit slider</h2>
        <?php

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $title = mysqli_real_escape_string($db->link, $_POST['title']);

            $permitted = array('jpg' , 'jpeg' , 'png' , 'gif');
            $file_Name = $_FILES['image']['name'];
            $file_Size = $_FILES['image']['size'];
            $file_Temp = $_FILES['image']['tmp_name'];
            $div = explode('.',$file_Name);
            $file_ext = strtolower(end($div));
            $unique_image = substr(md5(time()), 0 ,10).'.'.$file_ext;
            $uploaded_image = "upload/".$unique_image;
            if($title == ""){
                echo "<span class='err'>Input field should not be empty</span>";
            }else {
                if (!empty($file_Name)) {
                    if ($file_Size > 2048567) {
                        echo "<span class='err'>Image should be less then 2MB.</span>";
                    } elseif (in_array($file_ext, $permitted) === false) {
                        echo "<span class='err'>you can upload only :-" . implode(',', $permitted) . "</span>";
                    } else {
                        move_uploaded_file($file_Temp, $uploaded_image);
                        $query = "UPDATE slider 
                            SET 
                            title = '$title',
                            image = '$uploaded_image'
                           
                            WHERE  id ='$sliderID'
                          
                ";
                        $updated_rows = $db->update($query);
                        if ($updated_rows) {
                            echo "<span class='succes'>slider updated Successfully.</span>";
                        } else {
                            echo "<span class='err'>slider Not updated.</span>";
                        }
                    }
                } else {
                    $query = "UPDATE slider 
                            SET 
                             title = '$title'
                            WHERE  id ='$sliderID' ";
                    $updated_rows = $db->update($query);
                    if ($updated_rows) {
                        echo "<span class='succes'>slider updated Successfully.</span>";
                    } else {
                        echo "<span class='err'>slider Not updated.</span>";
                    }
                }


            }

        }
        ?>
        <div class="block">
            <?php
            $query = "SELECT  * FROM slider WHERE  id = '$sliderID' ";
            $sliderdata = $db->select($query);
            while ($sliderresult = $sliderdata->fetch_assoc()){

                ?>
                <form action="slideredit.php" method="post" enctype="multipart/form-data">
                    <br class="form">
                    <table>
                        <tr>
                            <td>
                                <label>Title</label>
                            </td>
                            <td>
                                <input type="text" name="title" value="<?php echo $sliderresult['title'];?>" class="medium" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label>Upload Image</label>
                            </td>
                            <td>
                                <img src="<?php echo $sliderresult['image'];?>" height="80px;" width="200px">
                                </br>
                                <input type="file" name="image" />
                            </td>
                        </tr>

                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php include "inc/footer.php";?>

