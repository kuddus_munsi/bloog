<?php
include "inc/header.php";
include "inc/sidebar.php";

if(!isset($_GET['editId']) && $_GET['editId'] == NULL){
    header("Location:edit.php");
}else{
    $id = $_GET['editId'];
}

?>

    <div class="grid_10">

        <div class="box round first grid">
            <h2>Update Category</h2>
<div class="block copyblock">
                <?php
                if($_SERVER['REQUEST_METHOD'] == 'POST'){
                    $name = $_POST['name'] ;
                    if(empty($name)){
                        echo "<span class='error'>Field must not be empty</span>";
                    }else {
                        $name = mysqli_real_escape_string($db->link, $name);
                        $query = "UPDATE category
                         SET
                         name = '$name'
                         WHERE id=$id";
                        $result = $db->update($query);
                        if ($result) {
                            echo "<span class='succes'>Data hasbeen successfully updated!</span>";
                        }
                        else{
                            echo "<span class='error'>Data has not been  updated</span>";
                        }
                    }
                }
                ?>
                <?php
                      $query = "SELECT * FROM category WHERE id = '$id'";
                      $category = $db->select($query);
                      while ($data = $category->fetch_assoc()){
                ?>

                <form action="" method="post">
                    <table class="form">
                        <tr>
                            <td>
                                <input type="text" name="name" value="<?php echo $data['name'] ;?>" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" name="submit" Value="Save" />
                            </td>
                        </tr>
                    </table>
                </form>

               <?php }
               ?>
            </div>
        </div>
    </div>
<?php include "inc/footer.php";?>