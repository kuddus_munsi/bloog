﻿<?php
include "inc/header.php";
include "inc/sidebar.php"
?>
    <div class="box round first grid">
        <h2>Update Social Media</h2>
        <div class="grid_10">
		<?php
if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $facebook = $format->validation( $_POST['facebook']);
    $twitter = $format->validation( $_POST['twitter']);
    $linkedin = $format->validation( $_POST['linkedin']);
    $googleplus = $format->validation( $_POST['googleplus']);

    $facebook = mysqli_real_escape_string($db->link, $facebook);
    $twitter = mysqli_real_escape_string($db->link, $twitter);
    $linkedin = mysqli_real_escape_string($db->link, $linkedin);
    $googleplus = mysqli_real_escape_string($db->link,$googleplus);

    if ($facebook == "" OR $twitter == ""  OR $linkedin == "" OR $googleplus == "") {
        echo "<span class='err'>Input field should not be empty</span>";
    } else {

        $query = "UPDATE social 
                            SET 
                            facebook = '$facebook',
                            twitter = '$twitter',
                            linkedin = '$linkedin',
                            google_plus = '$googleplus'
                            WHERE  id ='1'
                          
                ";
        $updated_rows = $db->update($query);
        if ($updated_rows) {
            echo "<span class='succes'>Post updated Successfully.</span>";
        } else {
            echo "<span class='err'>Post Not updated.</span>";
        }
    }
}
        ?>

                <div class="block">
                    <?php
                 $query = "SELECT * FROM social WHERE  id ='1'";
                    $social = $db->select($query);
                    if($social){
                        while ($data = $social->fetch_assoc()){

                    ?>
                 <form action="social.php" method="post">
                    <table class="form">					
                        <tr>
                            <td>
                                <label>Facebook</label>
                            </td>
                            <td>
                                <input type="text" name="facebook" value="<?php echo $data['facebook'];?>" class="medium" />
                            </td>
                        </tr>
						 <tr>
                            <td>
                                <label>Twitter</label>
                            </td>
                            <td>
                                <input type="text" name="twitter" value="<?php echo $data['twitter'];?>" class="medium" />
                            </td>
                        </tr>
						
						 <tr>
                            <td>
                                <label>LinkedIn</label>
                            </td>
                            <td>
                                <input type="text" name="linkedin" value="<?php echo $data['linkedin'];?>" class="medium" />
                            </td>
                        </tr>
						
						 <tr>
                            <td>
                                <label>Google Plus</label>
                            </td>
                            <td>
                                <input type="text" name="googleplus" value="<?php echo $data['google_plus'];?>"   class="medium" />
                            </td>
                        </tr>
						
						 <tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                    </form>
      <?php } } ?>
                </div>
            </div>
        </div>
<?php include "inc/footer.php";?>