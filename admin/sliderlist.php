<?php
include "inc/header.php";
include "inc/sidebar.php";
?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Slider  List</h2>
            <div class="block">
                <table class="data display datatable" id="example">
                    <thead>
                    <tr>
                        <th width="5%">NO.</th>
                        <th width="15%">Slider Title</th>
                        <th width="10%">image</th>
                        <th width="10%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $query = "SELECT * FROM slider ORDER  by id DESC ";
                    $allData = $db->select($query);
                    if ($allData){
                        $i=0;
                        while ($data = $allData->fetch_assoc()){
                            $i++;


                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $data['title'] ?></a></td>
                                <td><img src="<?php echo $data['image'] ?>" height="40px" width="40px" alt=""></td>

                                <td>
                                  <a href="slideredit.php?sliderid=<?php echo $data['id'];?>">Edit</a>||
                                  <a onclick="return confirm('are you sure to delete slider');" href="sliderdelete.php?postdeleteID=<?php echo $data['id']?>">Delete</a></td>
                        </tr>
                        <?php } }?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            setupLeftMenu();
            $('.datatable').dataTable();
            setSidebarHeight();
        });
    </script>
<?php include "inc/footer.php";?>