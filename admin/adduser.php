 <?php
include "inc/header.php";
include "inc/sidebar.php";

 if(Session::get('userRole') !== '3'){
  echo "<script>window.location = 'index.php';</script>";
 }
?>

    <div class="grid_10">

        <div class="box round first grid">
            <h2>Add New Category</h2>
            <div class="block copyblock">
                <?php
                if($_SERVER['REQUEST_METHOD'] == 'POST'){
                    $username = $format->validation($_POST['username']);
                    $password = $format->validation(md5($_POST['password']));
                    $userrole = $format->validation($_POST['userrole']);
                    $email = $format->validation($_POST['email']);

                    $username = mysqli_real_escape_string($db->link, $username);
                    $password = mysqli_real_escape_string($db->link, $password);
                    $userrole = mysqli_real_escape_string($db->link, $userrole);
                    $email = mysqli_real_escape_string($db->link, $email);

                    if(empty($username) || empty($password) || empty($userrole) || empty($email)){
                        echo "<span class='error'>Field must not be empty</span>";
                    }else {
                        $mailcheck = "select * from user WHERE email = '$email'";
                        $maildata = $db->select($mailcheck);
                        if($maildata != false){
                            echo "<span class='error'>This email is already exist</span>";
                        }else{
                        $query = "INSERT INTO user(username,password,email,role) VALUES('$username','$password','$email','$userrole')";
                        $result = $db->insert($query);
                        if ($result) {
                            echo "<span class='succes'>User hasbeen created successfully !</span>";
                        }
                        else{
                            echo "<span class='error'>User has not been created.</span>";
                        }
                    }
                }}
                ?>

                <form action="adduser.php" method="post">
                    <table class="form">
                        <tr>
                            <td>
                                <label for="">User Name:</label>
                            </td>
                            <td>
                                <input type="text" name="username" placeholder="Enter User Name..." class="medium" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label for="">Password:</label>
                            </td>
                            <td>
                                <input type="password" name="password" placeholder="Enter user password..." class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="">Email:</label>
                            </td>
                            <td>
                                <input type="email" name="email" placeholder="Enter your email." class="medium" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label for="">User Role:</label>
                            </td>
                            <td>
                                <select name="userrole" id="select">
                                    <option >Select User Role</option>
                                    <option value="1">Author</option>
                                    <option value="2">Editor</option>
                                    <option value="3">Admin</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <input type="submit" name="submit" Value="Create" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
<?php include "inc/footer.php";?>