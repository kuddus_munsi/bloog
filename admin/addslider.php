<?php
include "inc/header.php";
include "inc/sidebar.php"
?>
<div class="grid_10">

    <div class="box round first grid">
        <h2>Add New slider</h2>
        <?php

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $title = mysqli_real_escape_string($db->link, $_POST['title']);


            $permitted = array('jpg' , 'jpeg' , 'png' , 'gif');
            $file_Name = $_FILES['image']['name'];
            $file_Size = $_FILES['image']['size'];
            $file_Temp = $_FILES['image']['tmp_name'];
            $div = explode('.',$file_Name);
            $file_ext = strtolower(end($div));
            $unique_image = substr(md5(time()), 0 ,10).'.'.$file_ext;
            $uploaded_image = "upload/slider/".$unique_image;
            if($title == "" OR  $file_Name == ""){
                echo "<span class='err'>Input field should not be empty</span>";
            }
            elseif($file_Size > 2048567){
                echo "<span class='err'>Image should be less then 2MB.</span>";
            }
            elseif (in_array($file_ext , $permitted) === false){
                echo "<span class='err'>you can upload only :-".implode(',' , $permitted)."</span>";
            }
            else{
                move_uploaded_file($file_Temp ,$uploaded_image);
                $query = "INSERT INTO   slider (title, image) VALUES ('$title', '$uploaded_image')";
                $inserted_rows = $db->Insert($query);
                if($inserted_rows){
                    echo  "<span class='succes'>slider Uploaded Successfully.</span>";
                }
                else{
                    echo "<span class='err'>slider Not Uploaded.</span>";
                }
            }
        }
        ?>
        <div class="block">
            <form action="addslider.php" method="post" enctype="multipart/form-data">
                <table class="form">

                    <tr>
                        <td>
                            <label>Title</label>
                        </td>
                        <td>
                            <input type="text" name="title" placeholder="Enter slider Title..." class="medium" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Upload Image</label>
                        </td>
                        <td>
                            <input type="file" name="image" />
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="submit" Value="ADD" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php include "inc/footer.php";?>

