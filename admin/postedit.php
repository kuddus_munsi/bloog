<?php
include "inc/header.php";
include "inc/sidebar.php";
global $postID;
if(!isset($_GET['posteditID']) OR $_GET['posteditID'] == NULL){
    echo "<script>document.location='postlist.php';</script>";
}else {
    $postID = $_GET['posteditID'];
}
?>
<div class="grid_10">

    <div class="box round first grid">
        <h2>Edit Post</h2>
        <?php

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $title = mysqli_real_escape_string($db->link, $_POST['title']);
            $category = mysqli_real_escape_string($db->link, $_POST['category']);
            $tags = mysqli_real_escape_string($db->link, $_POST['tag']);
            $author = mysqli_real_escape_string($db->link, $_POST['author']);
            $body = mysqli_real_escape_string($db->link, $_POST['body']);
            $userid = mysqli_real_escape_string($db->link, $_POST['userid']);

            $permitted = array('jpg' , 'jpeg' , 'png' , 'gif');
            $file_Name = $_FILES['image']['name'];
            $file_Size = $_FILES['image']['size'];
            $file_Temp = $_FILES['image']['tmp_name'];
            $div = explode('.',$file_Name);
            $file_ext = strtolower(end($div));
            $unique_image = substr(md5(time()), 0 ,10).'.'.$file_ext;
            $uploaded_image = "upload/".$unique_image;
            if($title == "" OR $tags == "" OR $category == "" OR $body == "" OR $author == ""){
                echo "<span class='err'>Input field should not be empty</span>";
            }else {
                if (!empty($file_Name)) {
                    if ($file_Size > 1048567) {
                        echo "<span class='err'>Image should be less then 1MB.</span>";
                    } elseif (in_array($file_ext, $permitted) === false) {
                        echo "<span class='err'>you can upload only :-" . implode(',', $permitted) . "</span>";
                    } else {
                        move_uploaded_file($file_Temp, $uploaded_image);
                        $query = "UPDATE post 
                            SET 
                            category = '$category',
                            title = '$title',
                            body = '$body',
                            image = '$uploaded_image',
                            author = '$author',
                            tag = '$tags',
                            userid = '$userid'
                            WHERE  id ='$postID'
                          
                ";
                        $updated_rows = $db->update($query);
                        if ($updated_rows) {
                            echo "<span class='succes'>Post updated Successfully.</span>";
                        } else {
                            echo "<span class='err'>Post Not updated.</span>";
                        }
                    }
                } else {
                    $query = "UPDATE post 
                            SET 
                            category = '$category',
                            title = '$title',
                            body = '$body',
                            author = '$author',
                            tag = '$tags',
                            userid = '$userid'
                            WHERE  id ='$postID'
                          
                ";
                    $updated_rows = $db->update($query);
                    if ($updated_rows) {
                        echo "<span class='succes'>Post updated Successfully.</span>";
                    } else {
                        echo "<span class='err'>Post Not updated.</span>";
                    }
                }


            }

        }
        ?>
        <div class="block">
            <?php
                $query = "SELECT  * FROM post WHERE  id = '$postID' ORDER BY id DESC ";
                $postdata = $db->select($query);
                while ($postResult = $postdata->fetch_assoc()){

                ?>
            <form action="" method="post" enctype="multipart/form-data">
                <br class="form">
<table>
                    <tr>
                        <td>
                            <label>Title</label>
                        </td>
                        <td>
                            <input type="text" name="title" value="<?php echo $postResult['title'];?>" class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tags</label>
                        </td>
                        <td>
                            <input type="text" name="tag" value="<?php echo $postResult['tag'];?>" class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label> Author </label>
                        </td>
                        <td>
                            <input type="text" name="author" value="<?php echo Session::get('username')?>" class="medium" />

                            <input type="hidden" name="userid" value="<?php echo Session::get('userID')?>" class="medium" />

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Category</label>
                        </td>
                        <td>
                            <select id="select" name="category">
                                <option value="1">Category One</option>

                                <?php
                                $query = "SELECT * FROM category";
                                $catagory  =  $db->select($query);
                                if($catagory){
                                    while ($data = $catagory->fetch_assoc()){
                                        ?>
                                        <option
                                                <?php
                                                if($postResult['category'] == $data['id']){
                                                ?>
                                                    selected="selected"
                                                    <?php }?>
                                                value="<?php echo $data['id'] ?>"><?php echo $data['name'] ?>
                                        </option>
                                    <?php }}?>
                            </select>
                        </td>

                    </tr>



                    <tr>
                        <td>
                            <label>Upload Image</label>
                        </td>
                        <td>
                            <img src="<?php echo $postResult['image'];?>" height="80px;" width="200px">
                            </br>
                            <input type="file" name="image" />
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; padding-top: 9px;">
                            <label>Content</label>
                        </td>
                        <td>
                            <textarea class="tinymce" name="body"><?php echo $postResult['body'];?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="submit" Value="Save" />
                        </td>
                    </tr>
                </table>
            </form>
            <?php } ?>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php include "inc/footer.php";?>

