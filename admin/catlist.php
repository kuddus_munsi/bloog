﻿<?php
include "inc/header.php";
include "inc/sidebar.php"
?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Category List</h2>
                <?php
                if(isset($_GET['delete'])){
                    $delId = $_GET['delete'];
                    $query = "DELETE FROM category  WHERE  id = '$delId'";
                    $deleteCategory = $db->delete($query);
                    if ($deleteCategory) {
                        echo "<span class='succes'>Data hasbeen deleted!</span>";
                    }
                    else{
                        echo "<span class='error'>Data has not been  deleted</span>";
                    }
                }
                ?>

                <div class="block">        
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Category Name</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
                    <?php
                    $query = "SELECT * FROM  category ORDER BY id DESC";
                    $category = $db->select($query);
                    if($category){
                        $i=0;
                        while ($data = $category->fetch_assoc()){
                            $i++;
                    ?>
						<tr class="odd gradeX">
							<td><?php echo  $i;?></td>
							<td><?php echo  $data['name'];?></td>
							<td><a href="edit.php?editId=<?php echo $data['id']?>">Edit</a> || <a onclick="return confirm('Are sure to delete!!!')" href="?delete=<?php echo $data['id']?>">Delete</a></td>
						</tr>
                    <?php

                        }
                    }
                    ?>
					</tbody>
				</table>
               </div>
            </div>
        </div>

    <script type="text/javascript">

        $(document).ready(function () {
            setupLeftMenu();

            $('.datatable').dataTable();
            setSidebarHeight();


        });
    </script>
<?php include "inc/footer.php";?>