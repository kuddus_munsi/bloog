﻿<?php
include "inc/header.php";
include "inc/sidebar.php";

if(isset($_GET['seenid'])){
    $seenID = $_GET['seenid'];
}
?>

<div class="grid_10">
            <div class="box round first grid">
                <h2>Inbox</h2>
               <?php
               if(isset($_GET['seenid'])){
                $seenID = $_GET['seenid'];
                $query = "UPDATE contact
                            SET 
                            status = '1'
                            WHERE id='$seenID'";
                $seendata = $db->update($query);
                if($seendata){
                    echo "Message sent to seen table";
                }else{
                    echo  "Something went wrong";
                }

                }
                ?>
                <div class="block">        
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Name</th>
							<th>email</th>
							<th>Message</th>
							<th>date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
                    <?Php
                    $query = "SELECT * FROM contact WHERE status = '0' ORDER  BY id ASC ";
                    $contactInfo = $db->select($query);
                    if($contactInfo){
                        $i=0;
                        while ($data = $contactInfo->fetch_assoc()){
                            $i++;
                    ?>
						<tr class="odd gradeX">
							<td><?php echo  $i;?></td>
							<td><?php echo $data['first_name'].' '.$data['last_name'];?></td>
							<td><?php echo $data['email'];?></td>
							<td><?php echo $format->textShort($data['message'], 40);?></td>
							<td><?php echo $format->dateFormat($data['date']);?></td>
							<td>
                                <a href="viewmessage.php?viewid=<?php echo $data['id']; ?>">View</a> ||
                                <a href="replymessage.php?replyid=<?php echo $data['id']; ?>">Reply</a> ||
                                <a onclick="return confirm('Are sure that you seened it??');" href="?seenid=<?php echo $data['id']; ?>">seen</a>

                            </td>
						</tr>
                    <?php } } ?>
					</tbody>
				</table>
               </div>
            </div>

    <div class="box round first grid">
        <h2>Seen Message</h2>
        <?php
        if(isset($_GET['delid'])){
            $delId = $_GET['delid'];
            $query = "DELETE FROM contact  WHERE  id = '$delId'";
            $deleteCategory = $db->delete($query);
            if ($deleteCategory) {
                echo "<span class='succes'>Message hasbeen deleted!</span>";
            }
            else{
                echo "<span class='error'>Message has not been  deleted</span>";
            }
        }
        ?>

        <div class="block">
            <table class="data display datatable" id="example">
                <thead>
                <tr>
                    <th>Serial No.</th>
                    <th>Name</th>
                    <th>email</th>
                    <th>Message</th>
                    <th>date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?Php
                $query = "SELECT * FROM contact WHERE status = '1' ORDER  BY id ASC ";
                $contactInfo = $db->select($query);
                if($contactInfo){
                    $i=0;
                    while ($data = $contactInfo->fetch_assoc()){
                        $i++;
                        ?>
                        <tr class="odd gradeX">
                            <td><?php echo  $i;?></td>
                            <td><?php echo $data['first_name'].' '.$data['last_name'];?></td>
                            <td><?php echo $data['email'];?></td>
                            <td><?php echo $format->textShort($data['message'], 40);?></td>
                            <td><?php echo $format->dateFormat($data['date']);?></td>
                            <td>
                                <a onclick="return confirm('Are sure that you want to delte it?');" href="?delid=<?php echo $data['id']; ?>">Delete</a>


                            </td>
                        </tr>
                    <?php } } ?>
                </tbody>
            </table>
        </div>
    </div>

        </div>
    <script type="text/javascript">

        $(document).ready(function () {
            setupLeftMenu();

            $('.datatable').dataTable();
            setSidebarHeight();


        });
    </script>
<?php include "inc/footer.php";?>