<style>
    .deletepage{margin-left: 10px}
    .deletepage a {
        border: 1px solid #ddd;
        color: #444;
        cursor: pointer;
        font-size: 20px;
        padding: 2px 10px;
        background-color: #DDDDDD;
        font-weight: normal;
    }
</style>
<?php
include "inc/header.php";
include "inc/sidebar.php";
    global $id;
    if(!isset($_GET['pageid']) && $_GET['pageid'] == NULL){
        header("Location:index.php");
    }else{
        $id = $_GET['pageid'];
    }

?>


<div class="grid_10">

    <div class="box round first grid">
        <h2>Edit Page</h2>
        <?php

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $name = mysqli_real_escape_string($db->link, $_POST['name']);
            $body = mysqli_real_escape_string($db->link, $_POST['body']);


            if($name == "" OR $body == "" ){
                echo "<span class='err'>Input field should not be empty</span>";
            } else{

                $query = "UPDATE page 
                          SET 
                          name = '$name',
                          body = '$body'
                          WHERE id= '$id'";
                $update_rows = $db->Insert($query);
                if($update_rows){
                    echo  "<span class='succes'>Post updated Successfully.</span>";
                }
                else{
                    echo "<span class='err'>Post Not updated.</span>";
                }
            }

 }
        ?>
        <div class="block">
            <?php
            $query = "SELECT * FROM page WHERE  id='$id'";
            $pageName = $db->select($query);
            if($pageName){
            while ($data = $pageName->fetch_assoc()){
            ?>
            <form action="" method="post" >
                <table class="form">

                    <tr>
                        <td>
                            <label>Title</label>
                        </td>
                        <td>
                            <input type="text" name="name" value="<?php echo $data['name']; ?>" class="medium" />
                        </td>
                    </tr>

                    <tr>
                        <td style="vertical-align: top; padding-top: 9px;">
                            <label>Content</label>
                        </td>
                        <td>
                            <textarea class="tinymce" name="body">
                                <?php echo $data['body'];?>
                            </textarea>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="submit"  Value="Update" />
                           <span class="deletepage"><a onclick="return confirm('Are you sure to delete the Page');" href="deletepage.php?deletepageid=<?php echo $data['id'];?>">Delete</a></span>
                        </td>
                    </tr>
                </table>
            </form>
            <?php }} ?>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php include "inc/footer.php";?>

