<?php
include "../library/Session.php";
Session::checkSession();

include "../config/config.php";
include "../library/Database.php";
$db = new Database();

if(!isset($_GET['postdeleteID']) OR $_GET['postdeleteID'] == NULL){
    echo "<script>document.location='postlist.php';</script>";
}else {
    $postID = $_GET['postdeleteID'];

    $query = "SELECT * FROM post WHERE  id = '$postID'";
    $selectedPostData = $db->select($query);
    while ($data = $selectedPostData->fetch_assoc()){
        $deleteimage = $data['image'];
        unlink($deleteimage);

        $query = "DELETE FROM post WHERE  id = '$postID'";
        $deleteData = $db->delete($query);
        if($deleteData){
            echo "<script>alert('Post deleted successfully');</script>";
            echo "<script>document.location='postlist.php';</script>";
        }
        else{
            echo "<script>alert('Post not deleted ');</script>";
            echo "<script>document.location='postlist.php';</script>";
        }
    }
}