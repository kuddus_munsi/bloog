<?php
include "inc/header.php";
include "inc/sidebar.php";
global $postID;
if(!isset($_GET['viewpostid']) OR $_GET['viewpostid'] == NULL){
    echo "<script>document.location='postlist.php';</script>";
}else {
    $postID = $_GET['viewpostid'];
}
?>
<div class="grid_10">

    <div class="box round first grid">
        <h2>View Post</h2>
        <?php

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            echo "<script>window.location = 'postlist.php';</script>";

        }
        ?>
        <div class="block">
            <?php
            $query = "SELECT  * FROM post WHERE  id = '$postID' ORDER BY id DESC ";
            $postdata = $db->select($query);
            while ($postResult = $postdata->fetch_assoc()){

                ?>
                <form action="" method="post" enctype="multipart/form-data">
                    <br class="form">
                    <table>
                        <tr>
                            <td>
                                <label>Title</label>
                            </td>
                            <td>
                                <input type="text" readonly  value="<?php echo $postResult['title'];?>" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Tags</label>
                            </td>
                            <td>
                                <input type="text" readonly  value="<?php echo $postResult['tag'];?>" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label> Author </label>
                            </td>
                            <td>
                                <input type="text" readonly  value="<?php echo Session::get('username')?>" class="medium" />

                                <input type="hidden" name="userid" value="<?php echo Session::get('userID')?>" class="medium" />

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Category</label>
                            </td>
                            <td>
                                <select id="select" readonly="" ">
                                    <option value="1">Category One</option>

                                    <?php
                                    $query = "SELECT * FROM category";
                                    $catagory  =  $db->select($query);
                                    if($catagory){
                                        while ($data = $catagory->fetch_assoc()){
                                            ?>
                                            <option
                                                <?php
                                                if($postResult['category'] == $data['id']){
                                                    ?>
                                                    selected="selected"
                                                <?php }?>
                                                value="<?php echo $data['id'] ?>"><?php echo $data['name'] ?>
                                            </option>
                                        <?php }}?>
                                </select>
                            </td>

                        </tr>



                        <tr>
                            <td>
                                <label>Upload Image </label>
                            </td>
                            <td>
                                <img src="<?php echo $postResult['image'];?>" readonly height="100px;" width="200px">
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-top: 9px;">
                                <label>Content</label>
                            </td>
                            <td>
                                <textarea class="tinymce" readonly"><?php echo $postResult['body'];?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" name="submit" Value="OK" />
                            </td>
                        </tr>
                    </table>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php include "inc/footer.php";?>

