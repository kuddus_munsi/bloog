﻿<?php
include "inc/header.php";
include "inc/sidebar.php";
?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Post List</h2>
        <div class="block">
            <table class="data display datatable" id="example">
            <thead>
                <tr>
                    <th width="5%">NO.</th>
                    <th width="15%">Post Title</th>
                    <th width="15%">Description</th>
                    <th width="15%">Category</th>
                    <th width="10%">Tag</th>
                    <th width="10%">Author</th>
                    <th width="10%">image</th>
                    <th width="10%">Date</th>
                    <th width="10%">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $query = "SELECT post.* , category.name FROM post INNER JOIN category ON post.category = category.id ORDER BY post.title ASC ";
                $allData = $db->select($query);
                if ($allData){
                    $i=0;
                    while ($data = $allData->fetch_assoc()){
                        $i++;


                        ?>
                <tr class="odd gradeX">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $data['title'] ?></a></td>
                    <td><?php echo $format->textShort($data['body'],100);  ?></td>
                    <td><?php echo $data['name'] ?></td>
                    <td><?php echo $data['tag'] ?></td>
                    <td><?php echo $data['author'] ?></td>
                    <td><img src="<?php echo $data['image'] ?>" height="40px" width="40px" alt=""></td>
                    <td><?php echo $format->dateFormat($data['date']); ?></td>

                    <td>
                        <a href="viewpost.php?viewpostid=<?php echo $data['id']?>">View</a>||
                        <?php
                        if(Session::get('userID') == $data['userid'] OR Session::get('userRole') == '3'){
                         ?>
                       <a href="postedit.php?posteditID=<?php echo $data['id']?>">Edit</a>||

                        <a onclick="return confirm('are you sure to delete post');" href="postdelete.php?postdeleteID=<?php echo $data['id']?>">Delete</a></td>

                       <?php } ?>


                </tr>
            <?php }}?>
            </tbody>
        </table>

       </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();
        $('.datatable').dataTable();
        setSidebarHeight();
    });
</script>
<?php include "inc/footer.php";?>