<?php
include "inc/header.php";
include "inc/sidebar.php";
global $id ;
if(!isset($_GET['viewid']) && $_GET['viewid'] == NULL){
    header("Location:inbox.php");
}else{
    $id = $_GET['viewid'];
}

?>
?>
<div class="grid_10">

    <div class="box round first grid">
        <h2>View Message</h2>


       <div class="block">
            <form action="" method="post" >
               <?php

               $query = "SELECT * FROM contact WHERE  id = '$id' ";
                $contactInfo = $db->select($query);
                if($contactInfo){
                while ($data = $contactInfo->fetch_assoc()){
            ?>

                <table class="form">

                    <tr>
                        <td>
                            <label>Name </label>
                        </td>
                        <td>
                            <input type="text"  value="<?php echo $data['first_name'].' '.$data['last_name'];?>" class="medium" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Email</label>
                        </td>
                        <td>
                            <input type="text"class="medium" value="<?php echo $data['email']; ?>" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Date</label>
                        </td>
                        <td>
                            <input type="text"  class="medium" value="<?php echo $format->dateFormat($data['date']); ?>" />
                         </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Content</label>
                        </td>
                        <td>
                            <textarea class="tinymce"><?php echo $data['message']; ?> </textarea>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <button class="btn btn-success" ><a href="inbox.php">OK!!</a>  </button>
                        </td>
                    </tr>
                </table>
                <?php } } ?>
            </form>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<?php include "inc/footer.php";?>

