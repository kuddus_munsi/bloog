<?php
include "../library/Session.php";
Session::checkSession();

include "../config/config.php";
include "../library/Database.php";
$db = new Database();

if(!isset($_GET['postdeleteID']) OR $_GET['postdeleteID'] == NULL){
    echo "<script>document.location='postlist.php';</script>";
}else {
    $sliderID = $_GET['postdeleteID'];

    $query = "SELECT * FROM slider WHERE  id = '$sliderID'";
    $selectedsliderData = $db->select($query);
    while ($data = $selectedsliderData->fetch_assoc()){
        $deleteimage = $data['image'];
        unlink($deleteimage);

        $query = "DELETE FROM slider WHERE  id = '$sliderID'";
        $deleteData = $db->delete($query);
        if($deleteData){
            echo "<script>alert('slider deleted successfully');</script>";
            echo "<script>document.location='sliderlist.php';</script>";
        }
        else{
            echo "<script>alert('slider not deleted ');</script>";
            echo "<script>document.location='sliderlist.php';</script>";
        }
    }
}