<?php
include "inc/header.php";
include "inc/sidebar.php";

$format = new  Format();
$db = new Database();
?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Category List</h2>
            <?php
            if(isset($_GET['delete'])){
                $deluser = $_GET['delete'];
                $query = "DELETE FROM user  WHERE  id = '$deluser'";
                $deleteuser= $db->delete($query);
                if ($deleteuser) {
                    echo "<span class='succes'>user hasbeen deleted!</span>";
                }
                else{
                    echo "<span class='error'>user has not been  deleted</span>";
                }
            }
            ?>

            <div class="block">
                <table class="data display datatable" id="example">
                    <thead>
                    <tr>
                        <th>Serial No.</th>
                        <th> Name</th>
                        <th> UserName</th>
                        <th> Email</th>
                        <th> Details</th>
                        <th> Role</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $query = "SELECT * FROM  user ORDER BY id DESC";
                    $alluser = $db->select($query);
                    if($alluser){
                        $i=0;
                        while ($data = $alluser->fetch_assoc()){
                            $i++;
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo  $i;?></td>
                                <td><?php echo  $data['name'];?></td>
                                <td><?php echo  $data['username'];?></td>
                                <td><?php echo  $data['email'];?></td>
                                <td><?php echo  $format->textShort($data['details']);?></td>

                                <td>
                                    <?php
                                           if($data['role'] == 1){
                                               echo "Author";
                                           }
                                    elseif($data['role'] == 2){
                                        echo "Editor";
                                    } elseif($data['role'] == 3){
                                        echo "Admin";
                                    }
                                    ?>
                                </td>
                                <td><a href="viewuser.php?viewid=<?php echo $data['id']?>">View</a> || <a onclick="return confirm('Are sure to delete!!!')" href="?delete=<?php echo $data['id']?>">Delete</a></td>
                            </tr>
                            <?php

                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            setupLeftMenu();

            $('.datatable').dataTable();
            setSidebarHeight();


        });
    </script>
<?php include "inc/footer.php";?>