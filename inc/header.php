<!doctype html>
<html class="no-js" lang="">
<head>

<?php
include 'script/meta.php';
include 'script/css.php';
include 'script/js.php';

?>



    <?php
    if (isset($_GET['pageID'])){
        $pagid = $_GET['pageID'];
        $query = "SELECT * FROM page WHERE  id='$pagid'";
        $pageName = $db->select($query);
        if($pageName){
            while ($data = $pageName->fetch_assoc()){
                ?>

                <title> <?php echo $data['name']; ?> - <?php echo TITLE ; ?></title>
            <?php }}} elseif (isset($_GET['id'])){
        $id = $_GET['id'];
    $query = "SELECT * FROM post WHERE  id='$id'";
    $postName = $db->select($query);
    if($postName){
    while ($data = $postName->fetch_assoc()){
        ?>
        <title> <?php echo $data['title']; ?> - <?php echo TITLE ; ?></title>
    <?php }}}else{?>
        <title> <?php echo $format->title();?> <?php echo TITLE ; ?></title>
    <?php }?>
    <style>
        #active{
            background-color: #fff;
            color:#333;
            border-bottom: 2px solid blue;}
    </style>

</head>
<body>

<section class=" headersection container clearr">
    <div class="row clearr">
        <div class="col-md-4">
            <div class="logo clearr">
                <?php
                $query = "SELECT * FROM title_slogan WHERE  id = '1'";
                $allData = $db->select($query);
                if($allData){
                    while ($data = $allData->fetch_assoc()){

                        ?>
                        <a href="index.php"> <img src="admin/<?php echo $data['logo'];?>" alt="LOGO"></a>
                    <?php }}?>
            </div>
        </div>
        <div class="col-md-4 col-md-6">
            <div class="social clearr">
                <?php
                $query = "SELECT * FROM social WHERE  id ='1'";
                $social = $db->select($query);
                if($social){
                while ($data = $social->fetch_assoc()){

                ?>
                <a href="<?php echo $data['facebook'];?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="<?php echo $data['twitter'];?>"><i class="fa fa-twitter" aria-hidden="true"></i>
                    <a href="<?php echo $data['linkedin'];?>"><i class="fa fa-linkedin" aria-hidden="true"></i>
                        <a href="<?php echo $data['google_plus'];?>"><i class="fa fa-google-plus" aria-hidden="true"></i>
                            <?php }}?>
            </div>
        </div>
    </div>
</section>

<section class="navbar clearr">
    <div class="container clearr">
        <div class="row clearr">
            <div class="navsection clearr">
                <div class="mainmenu">
                    <?php
                    $path = $_SERVER['SCRIPT_NAME'];
                    $currentPage = basename($path , '.php');
                    ?>
                    <ul>
                        <li ><a
                                    <?php if($currentPage == 'index'){  echo 'id="active"'; }?>
                                    href="index.php">Home</a></li>
                        <?php
                        $query = "SELECT * FROM  page";
                        $allData = $db->select($query);
                        if($allData){
                            while ($data = $allData->fetch_assoc()){
                                ?>

                                <li>
                                    <a
                                            <?php
                                            if(isset($_GET['pageID']) && $_GET['pageID'] ==  $data['id'] ){
                                                echo 'id="active"';
                                            }
                                            ?>
                                            href="page.php?pageID=<?php echo $data['id']?>">
                                        <?php echo $data['name'];?>
                                    </a>
                                </li>
                            <?php }} ?>
                        <li><a
                                <?php if($currentPage == 'contact'){  echo 'id="active"'; }?>
                                    href="contact.php">Contact</a></li>
                    </ul>
                </div>
                <div class="search clearr">
                    <form class="navbar-form navbar-left clearr" method="post" action="search.php">
                        <div class="form-group">
                            <input style="padding:3px" type="text" class="form-control" name="search" placeholder="Search">
                            <button type="submit" class="btn btn-default">Search</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

