
<section class="footertop clearr">
    <div class="container clearr">
        <div class="row">
            <div class=" mainaddress clearr">
                <div class="col-md-6">
                    <div class="address clearr">
                        <h1>Physical Location</h1>
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> Zakir Hossain Rd, Chittagong, Bangladesh</p>
                        <p><i class="fa fa-laptop" aria-hidden="true"></i></i> Faculty of Science, Engineering and Technology</p>
                        <p><i class="fa fa-university" aria-hidden="true"></i></i> University of Science and Technology Chittagong</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="address clearr">
                        <h1>Basic contact</h1>
                        <p><i class="fa fa-envelope-o" aria-hidden="true"> xyz@xyz.com</i></p>
                        <p><i class="fa fa-mobile" aria-hidden="true"></i> +8801234567890</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="footermid clearr">
    <div class="container clearr">
        <div class="row">
            <div class="footer_mid_navbar clearr">
                <div class="col-md-6">
                    <div class="footermenu clearr">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="about.php">About</a></li>
                            <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="footersocial clearr">
                        <?php
                        $query = "SELECT * FROM social WHERE  id ='1'";
                        $social = $db->select($query);
                        if($social){
                        while ($data = $social->fetch_assoc()){

                        ?>
                        <a href="<?php echo $data['facebook'];?>"><img class="img-responsive" src="img/fb.png" title="Facebook" alt="Facebook" class="img-circle"></a>
                        <a href="<?php echo $data['twitter'];?>"><img class="img-responsive" src="img/twt.png"  title="Twitter" alt=" Twitter" class="img-circle"></a>
                        <a href="<?php echo $data['linkedin'];?>"><img class="img-responsive" src="img/link.png" title="Linkedin" alt="Linkedin" class="img-circle"></a>
                        <a href="google_plus"><img class="img-responsive" src="img/ggl.png" title="Google Plus" alt="Google plus" class="img-circle"></a>
                        <?php }}?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mainbottom clearr">
    <footer class= "container">
        <div class="row">
            <div class="copyright">
                <?php
                $query = "SELECT * FROM  copyright WHERE  id = '1'";
                $allData = $db->select($query);
                if($allData){
                while ($data = $allData->fetch_assoc()){

                ?>
                <p> &copy;  <?php echo $data['text'];?>  <?php echo date('Y');?></p>
                <?php }} ?>
            </div>
        </div>
    </footer>
</section>

<!--scroll to top-->
<button onclick="topFunction()" id="myBtn" title="top">TOP</button>
<script>
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("myBtn").style.display = "block";
        } else {
            document.getElementById("myBtn").style.display = "none";
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
</script>
</body>
</html>
