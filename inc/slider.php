
<section class=" slider clearr">
    <div class="container clearr">
        <div class="row clearr">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">

                    <div class="item active">
                        <img src="img/i1.jpg" alt="Chania" width="460" height="345">
                        <div class="carousel-caption">
                            <p>Vatiyari Tour 2015</p>
                        </div>
                    </div>

                    <div class="item">
                        <img src="img/i2.jpg" alt="Chania" width="460" height="345">
                        <div class="carousel-caption">
                            <p>Vatiyari Award Time 2015</p>
                        </div>
                    </div>

                    <div class="item">
                        <img src="img/i3.jpg" alt="Flower" width="460" height="345">
                        <div class="carousel-caption">
                            <p>Kaptai Batch Day 2017</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="img/i3.jpg" alt="Flower" width="460" height="345">
                        <div class="carousel-caption">
                            <p>Kaptai Batch Day 2017</p>
                        </div>
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>
