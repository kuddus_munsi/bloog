<?php
include "helper/Format.php";
include "config/config.php";
include "library/Database.php";

global $db;
$format = new  Format();
$db = new Database();
include "inc/header.php";
include "inc/slider.php";
?>


    <section class="maincontentsection container  clearr">
        <div class=" container">
        <div class="row">
            <div class="col-md-8">
                <div class="maincontent clearr">

                    <?php
                   /*helper for pagination start*/
                   $per_page = 3;
                    if(isset($_GET['page'])){
                        $page = $_GET['page'];
                    }else{
                        $page = 1;
                    }
                    $start_from = ($page-1)*$per_page;
                    /*helper for pagination end*/


                    $query = "SELECT * FROM post limit $start_from, $per_page";
                        $post = $db->select($query);
                        if($post){
                            while ($data = $post->fetch_assoc()){
                     ?>
                    <div class="contentsection clearr">
                        <h2> <a href="post.php?id=<?php echo  $data['id']; ?>"> <?php echo  $data['title']; ?> </a> </h2>
                        <h4> <?php echo $format->dateFormat($data['date']) ; ?>  By <a href="#">  <?php echo  $data['author']; ?> </a></h4>
                        <img src="admin/<?php echo  $data['image'];?>" alt="Article image" class="img-thumbnail">
                        <p><?php echo $format->textShort($data['body'])   ;?></p>
                        <div class="readmore clearr"><a href="post.php?id=<?php echo  $data['id']; ?>"><p> Read More &gt&gt</p></a></div>
                    </div>
                    <?php
                    }
                    /*pagination start*/
                    $query1 = "SELECT * FROM post";
                    $result = $db->select($query1);
                    $total_rows = mysqli_num_rows($result);
                    $total_page = ceil($total_rows/$per_page);
                    echo "<span class='pagination'><a href='index.php?page=1'>".'First Page'."</a>";
                        for ($i = 1;$i<=$total_page;$i++){
                            echo "<a href='index.php?page=".$i."'>".$i."</a>";
                        }
                    echo "<a href='index.php?page=$total_page'>".'First Page'."</a></span>";
                    /*pagination end */
                        }else{
                            echo header("Location:404.php");
                        }
                    ?>
                </div>

         </div>

<?php
include "inc/sidebar.php";
include "inc/footer.php";
?>