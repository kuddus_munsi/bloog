<?php
include "helper/Format.php";
include "config/config.php";
include "library/Database.php";

$format = new  Format();
$db = new Database();

include "inc/header.php";

if(!isset($_POST['search']) OR $_POST['search'] == NULL){
    header("Location:404.php");
}else{
    $search = $_POST['search'];
}
?>
<section class="maincontentsection container  clearr">
    <div class=" container">
        <div class="row">
            <div class="col-md-8">
                <div class="maincontent clearr">
                    <?php
                    $query  = "SELECT * FROM post WHERE title LIKE '%$search%' OR body LIKE '%$search%' ";
                    $serachResultt = $db->select($query);
                    if($serachResultt){
                        while ($data = $serachResultt->fetch_assoc()){
                            ?>
                            <div class="contentsection clearr">
                                <h2> <?php echo  $data['title']; ?> </h2>
                                <h4> <?php echo $format->dateFormat($data['date']) ; ?>  By  <a href="#"> <?php echo  $data['author']; ?> </a> </h4>
                                <img src="admin/<?php echo  $data['image'];?>" alt="Article image" class="img-thumbnail">
                                <?php echo $data['body']   ;?>
                            </div>
                            <?php
                        }}else{
                        echo "<p>Your search query is not found</p>";
                    }

                    ?>
                </div>
            </div>

            <?php
            include "inc/sidebar.php";

            $querycat  = "SELECT * FROM post";
            $catdata =  $db->select($querycat);
            $relatedResult = $catdata->fetch_assoc();
            $catID = $relatedResult['category'];
            ?>
            <section class="mainservice clearr">
                <div class="container clearr">
                    <div class="row">

                        <div class="servicesection clearr">
                            <div class="notification">
                                <h1> Related Post</h1>
                            </div>
                            <?php

                            $catQuery  = "SELECT * FROM post WHERE category=$catID ORDER BY rand() limit 6";
                            $relCatagory =  $db->select($catQuery);
                            if($relCatagory){
                                while ($categoryData = $relCatagory->fetch_assoc()){
                                    ?>

                                    <a href="post.php?id=<?php echo  $categoryData['id']; ?>">
                                        <img   src="admin/upload/<?php echo  $categoryData['image']; ?>" class="imageresige" >
                                    </a>

                                    <?php
                                }}else{
                                echo "No related post available";
                            }

                            ?>
                        </div>



                    </div>
                </div>
            </section>
            <?php
            include "inc/footer.php";
            ?>
            <!--

             -->