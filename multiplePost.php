<?php
include "helper/Format.php";
include "config/config.php";
include "library/Database.php";

$format = new  Format();
$db = new Database();

include "inc/header.php";
include "inc/slider.php"
?>


    <section class="maincontentsection container  clearr">
    <div class=" container">
    <div class="row">
    <div class="col-md-8">
        <div class="maincontent clearr">

            <?php
            global $categoryID;
             if(!isset($_GET['category']) OR $_GET['category'] == NULL){
                return header("Location:404.php");
            }else{
                 $categoryID = $_GET['category'];
            }


            $query = "SELECT * FROM post WHERE category=$categoryID";
            $category = $db->select($query);
            if($category){
                while ($data = $category->fetch_assoc()){
                    ?>
                    <div class="contentsection clearr">
                        <h2> <a href="post.php?id=<?php echo  $data['id']; ?>"> <?php echo  $data['title']; ?> </a> </h2>
                        <h4> <?php echo $format->dateFormat($data['date']) ; ?>  By <a href="#">  <?php echo  $data['author']; ?> </a></h4>
                        <img src="admin/<?php echo  $data['image'];?>" alt="Article image" class="img-thumbnail">
                        <?php echo $format->textShort($data['body'])   ;?>
                        <div class="readmore clearr"><a href="post.php?id=<?php echo  $data['id']; ?>"><p> Read More &gt&gt</p></a></div>
                    </div>
            <?php
                }
                }else{
                echo "<br>";
                   echo "<span style='color: red; font-size: 40px;margin: 50px;padding: 100px;'> Data not found </span>";
            }
            ?>
        </div>

    </div>

<?php
include "inc/sidebar.php";
include "inc/footer.php";
?>