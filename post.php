<?php
include "helper/Format.php";
include "config/config.php";
include "library/Database.php";
$format = new  Format();
$db = new Database();
include "inc/header.php";
if(!isset($_GET['id']) OR $_GET['id'] == NULL){
    header("Location:404.php");
}else{
    $id = $_GET['id'];
}
?>
    <section class="maincontentsection container  clearr">
        <div class=" container">
            <div class="row">
                <div class="col-md-8">
                    <div class="maincontent clearr">
                        <?php
                        $query  = "SELECT * FROM post WHERE id=$id";
                        $post = $db->select($query);
                        if($post){
                            while ($data = $post->fetch_assoc()){
                        ?>
                        <div class="contentsection clearr">
                            <h2> <?php echo  $data['title']; ?> </h2>
                            <h4> <?php echo $format->dateFormat($data['date']) ; ?>  By  <a href="#"> <?php echo  $data['author']; ?> </a> </h4>
                            <img src="admin/<?php echo  $data['image'];?>" alt="Article image" class="img-thumbnail">
                            <article style="line-height: 16px;font-size: 14px"> <?php echo $data['body']; ?> </article>
                        </div>
                        <?php
                        }}else{
                            echo header("Location:404.php");
                        }

                        ?>
                    </div>
                </div>

<?php
include "inc/sidebar.php";

$querycat  = "SELECT * FROM post";
$catdata =  $db->select($querycat);
$relatedResult = $catdata->fetch_assoc();
$catID = $relatedResult['category'];
?>
<section class="mainservice clearr">
<div class="container clearr">
    <div class="row">

            <div class="servicesection clearr">
                <div class="notification">
                    <h1> Related Post</h1>
                </div>
                <?php

                $catQuery  = "SELECT * FROM post WHERE category=$catID ORDER BY rand() limit 6";
                $relCatagory =  $db->select($catQuery);
                if($relCatagory){
                    while ($categoryData = $relCatagory->fetch_assoc()){
                        ?>

                        <a href="post.php?id=<?php echo  $categoryData['id']; ?>">
                            <img   src="admin/<?php echo  $categoryData['image']; ?>" class="imageresige" >
                        </a>

                        <?php
                    }}else{
                    echo "No related post available";
                }

                ?>
            </div>



    </div>
</div>
</section>
<?php
include "inc/footer.php";
?>
               <!--

                -->