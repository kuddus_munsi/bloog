<?php
include "helper/Format.php";
include "config/config.php";
include "library/Database.php";

$format = new  Format();
$db = new Database();

include "inc/header.php";
include "inc/slider.php";
?>
    <section class="maincontentsection container  clearr">
        <div class=" container">
            <div class="row">
                <div class="col-md-8">
                    <div class="maincontent clearr">
                        <div class="contentsection clearr">
                            <div class="notfound">
                                <p><span>404</span> Not Found</p>
                            </div>

                        </div>

                    </div>
                </div>

<?php
include "inc/sidebar.php";
include "inc/footer.php";
?>