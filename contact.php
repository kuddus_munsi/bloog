<?php
include "helper/Format.php";
include "config/config.php";
include "library/Database.php";

$format = new  Format();
$db = new Database();

include "inc/header.php";

?>

<?php
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
                $fName = $format->validation($_POST['fname']);
                $lName = $format->validation($_POST['lname']);
                $email = $format->validation($_POST['email']);
                $message = $format->validation($_POST['message']);


                $fName = mysqli_real_escape_string($db->link, $fName);
                $lname = mysqli_real_escape_string($db->link, $lName);
                $email = mysqli_real_escape_string($db->link, $email);
                $message = mysqli_real_escape_string($db->link, $message);
            $error = "";
              if(empty($fName)){
                  $error = "First name must not be empty";
              }
              elseif(empty($lName)){
                $error = "Last  name must not be empty";
            }elseif(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                $error = "Invalid emil address";
              }elseif(empty($message)){
                $error = "message must not be empty";
              }
                else{
                   $query = "INSERT INTO contact(first_name,last_name,email,message)VALUES ('$fName','$lname','$email','$message')";
                   $contact = $db->insert($query);
                   if($contact){
                       $success = "Message hasbeen sent successfully";
                   }
                   else{
                       $error = "message has not been sent";
                   }
                }

            }
?>
    <section class="maincontentsection container  clearr">
        <div class=" container">
        <div class="row">
            <div class="col-md-8">
                <div class="contactus">
                    <h1>Contact form</h1><br>
                    <?php
                    if(isset($error)){

                      echo "<span style='color: red'> $error</span>";
                    }
                    if(isset($success)){
                        echo  "<span style='color: green'> $success </span>";
                    }
                    ?>
                    <br>
                <form class="form-group" action="" method="post">
                    <table>
                        <tr>

                               <td><label for="fname">Your First Name:</label></td>
                                <td><input type="text" name="fname" id="fname" class="form-control"></td>

                        </tr>
                        <tr>

                            <td><label for="lname">Your  Last  Name:</label></td>
                            <td><input type="text" name="lname" id="lname" class="form-control"></td>

                        </tr>

                        <tr>

                            <td><label for="email">Your Email:</label></td>
                            <td><input type="email" name="email" id="email" class="form-control"></td>

                        </tr>

                        <tr>

                            <td><label for="name">Your Messages:</label></td>
                            <td><textarea  name="message" id="name" class="form-control"></textarea></td>

                        </tr>
                        <tr >

                            <td ><input type="submit" name="submit" value="Send" class="btn btn-success form-control"></td>

                        </tr>




                    </table>

                </form>
                </div>
</div>

<?php
include "inc/sidebar.php";
include "inc/footer.php";
?>